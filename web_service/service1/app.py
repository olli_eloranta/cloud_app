import time

from flask import Flask
from flask import request
import requests

app = Flask(__name__)

def ask_service2():
    return

@app.route('/')
def hello():
    msg = requests.get("http://s2:5000/").text
    msg2 = f"Hello from {request.environ['REMOTE_ADDR']}:{request.environ['REMOTE_PORT']} to {request.host}"
    return {"Service 1": msg2, 
            "Service 2": msg}

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8001)